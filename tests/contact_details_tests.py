import unittest
from models.phone import Phone
from models.adress import Adress
from models.email import Email
from models.website import WebSite
from models.contact import Contact


class TestValidateMethods(unittest.TestCase):

    def test_valid_phone_number(self):
        contact_phone = Phone("06 06 06 06 06", Contact("test"))
        self.assertEqual(contact_phone.get_value(), "06 06 06 06 06")

    def test_invalid_phone(self):
        with self.assertRaises(Exception):
            Phone("invalid phone", Contact("test"))

    def test_valid_email(self):
        contact_email = Email("test.test@gmail.com", Contact("test"))
        self.assertEqual(contact_email.value, "test.test@gmail.com")

    def test_invalid_email(self):
        with self.assertRaises(Exception):
            Email("myinvalidmail.com", Contact("test"))

    def test_valid_adress(self):
        contact_adress = Adress("My Adress is more than 10 char and do not contain any special chars", Contact("test"))
        self.assertEqual(contact_adress.value, "My Adress is more than 10 char and do not contain any special chars")

    def test_invalid_adress_to_short(self):
        with self.assertRaises(Exception):
            Adress("to short", Contact("test"))

    def test_invalid_adress_special_chars(self):
        self.assertRaises(Exception, Adress("My adress contains special chars @#!/;&", Contact("test")))

    def test_valid_website(self):
        contact_website = WebSite("https://mywebsite.fr", Contact("test"))
        self.assertEqual(contact_website.value, "https://mywebsite.fr")

    def test_invalid_website(self):
        with self.assertRaises(Exception):
            WebSite("//mywebsite.com", Contact("test"))
