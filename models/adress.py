from typing import Any

from models.contact_details import ContactDetails
from models.contact import Contact


class Adress(ContactDetails):
    REGEX = r"[\w\d\s,-]{10,50}"

    def __init__(self, value, owner: Contact):
        super().__init__(value, self.REGEX, owner)

    def __repr__(self):
        return f"adress: {self.value}"