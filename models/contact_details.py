import re

class ContactDetails:

    def __init__(self, value: str, regex: str, owner):
        self.owner = owner
        self._regex = regex
        self.value = self.set_value(value)

    def set_value(self, value):
        self.validate(value)
        self.value = value

    def get_value(self):
        return self.value

    def validate(self, value):
        validated = re.search(self._regex, value)
        if not validated:
            raise Exception("Invalid value")