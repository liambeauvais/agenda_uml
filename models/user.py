from models.agenda import Agenda


class User:

    def __init__(self, login: str, password: str, jwt: str, agendas: list[Agenda] = None):
        """Create a new user initialized with one Agenda"""
        self.login = login
        self._password = password
        self._jwt = jwt

        if agendas is not None and len(agendas) > 0:
            self.agendas = agendas
        else:
            self.agendas = list[Agenda]

    def get_password(self):
        """Return user password"""
        return self._password

    def get_jwt(self):
        """Return user json web token"""
        return self._jwt

    def __repr__(self):
        return f"user: {self.login}"

