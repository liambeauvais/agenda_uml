from models.contact_details import ContactDetails
from models.contact import Contact

class Email(ContactDetails):
    REGEX = r'([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+'

    def __init__(self, value, owner: Contact):
        super().__init__(value, self.REGEX, owner)

    def __repr__(self):
        return f"email: {self.value}"
