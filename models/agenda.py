from models.contact import Contact


class Agenda:

    def __init__(self, contacts: list[Contact] = None):
        """Init an agenda with a list of contact if specified"""
        self._contacts : list[Contact] = []
        if contacts is not None and len(contacts) > 0:
            self._contacts = contacts

    def get_contacts(self):
        """Return contact list"""
        return self._contacts

    # def set_contacts(self, value: list[Contact]):
    # self._contacts = value

    def add_contact(self, new_contact: Contact):
        """Add given contact to contact list"""
        self._contacts.append(new_contact)

    def remove_contact(self, contact: Contact):
        """Remove given contact from contact list"""
        self._contacts.remove(contact)

    def remove_all_contacts(self):
        """Remove all contacts from contact list"""
        self._contacts.clear()

    def __repr__(self):
        return f"agenda"