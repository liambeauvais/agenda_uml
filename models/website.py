from models.contact_details import ContactDetails
from models.contact import Contact

class WebSite(ContactDetails):
    REGEX = r"https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)"

    def __init__(self, value, owner: Contact):
        super().__init__(value, self.REGEX, owner)

    def __repr__(self):
        return f"site url: {self.value}"


