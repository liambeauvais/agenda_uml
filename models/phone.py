import re

from models.contact_details import ContactDetails
from models.contact import Contact

columns = ['value']

class Phone(ContactDetails):
    REGEX = r"(\d{2} {1}){4}\d{2}"

    def __init__(self, value, owner: Contact):
        super().__init__(value, self.REGEX, owner)

    def __repr__(self):
        return f"phone num: {self.value}"