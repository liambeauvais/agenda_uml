from models.contact_details import ContactDetails


class Contact:

    def __init__(self, name: str):
        """Init a contact with a name and his details if specified"""
        self._addresses = []
        self._name = name

    def get_addresses(self):
        """Return contact's details"""
        return self._addresses

    def add_address(self, contact_detail: ContactDetails):
        """Add given detail to contact's details"""
        self._addresses.append(contact_detail)

    def remove_address(self, contact_detail: ContactDetails):
        """Remove given detail from contact's details"""
        self._addresses.remove(contact_detail)

    def get_name(self):
        """Set contact name"""
        return self._name

    def set_name(self, name: str):
        """Return contact name"""
        self._name = name

    def __repr__(self):
        f"contact {self._name}"