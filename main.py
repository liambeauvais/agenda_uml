import sys

from models.adress import Adress
from models.agenda import Agenda
from models.contact import Contact
from models.contact_details import ContactDetails
from models.email import Email
from models.phone import Phone
from models.user import User
from models.website import WebSite

user = User(login="liam", password="password", jwt="jwt")

contact_malo = Contact(name="malo")
contact_malo.add_address(Phone(value="06 23 23 23 23", owner=contact_malo))
contact_malo.add_address(Adress(value="1, rue des exemples", owner=contact_malo))
contact_malo.add_address(Email(value="test.test@test.com", owner=contact_malo))
contact_malo.add_address(WebSite(value="https://stackoverflow.com", owner=contact_malo))

contact_titouan = Contact(name="titouan")

print(contact_malo.get_addresses())
agenda = Agenda()
agenda.add_contact(contact_malo)
agenda.add_contact(contact_titouan)

user.agendas = agenda


def show_contacts(contacts):
    for contact in contacts:
        print(f"{contact.get_name()}:{contact.get_addresses()}")


def show_contact_options(agenda: Agenda):
    show_contacts(agenda.get_contacts())
    index = -1
    while not (0 <= index < len(agenda.get_contacts())):
        index = int(input("""Lequel voulez-vous modifier?
                (PS: Marquez 0 pour le premier item, etc..."""))
    contact = agenda.get_contacts()[index]
    print(f"Vous voulez modifier le contact {contact.get_name()}")
    while True:
        value = input("""ACTIONS:
            1.ajouter un moyen de contact
            2.Modifier un moyen de contact
            3.Supprimer un moyen de contact
            4.Montrer mes moyens de contacts
            5.Go back 
            """)

        if value == "1":
            while True:
                type = input("Type de moyen de contact? (Phone, Email, WebSite, Adress)")
                name = input("Quelle est la valeur de votre moyen de contact?")
                if name != "" and type in ["Phone", "Email", "WebSite", "Adress"]:
                    break
            moyen_de_contact = getattr(sys.modules[__name__], type)
            try:
                contact.add_address(moyen_de_contact(value=name, owner=contact))
            except:
                continue
            print("Moyen de contact ajouté")
        if value == "2":
            moyens = contact.get_addresses()
            for moyen in moyens:
                print(moyen)
            index = -1
            while not (0 <= index < len(contact.get_addresses())):
                index = int(input("""Lequel voulez-vous supprimer?
                                                        (PS: Marquez 0 pour le premier item, etc..."""))
            while True:
                try :
                    moyen : ContactDetails = contact.get_addresses()[index]
                    print(f"modif de votre {moyen}")
                    value = input("Valeur pour modification de votre moyen")
                    moyen.set_value(value)
                    break
                except:
                    continue

        if value == "3":
            index = -1
            while not (0 <= index < len(contact.get_addresses())):
                index = int(input("""Lequel voulez-vous modifier?
                                        (PS: Marquez 0 pour le premier item, etc..."""))
            moyen = contact.get_addresses()[index]
            print(f"Vous voulez supprimer le moyen de contact {moyen}")
            contact.remove_address(moyen)
            print(f"moyen de contact supprimé")

        if value == "4":
            moyens = contact.get_addresses()
            for moyen in moyens:
                print(moyen)

        if value == "5":
            print("Back to agenda!")
            return


def show_agenda():
    print("Voici ton agenda:")
    agenda: Agenda = user.agendas
    show_contacts(agenda.get_contacts())
    while True:
        value = input("""ACTIONS:
        1.ajouter un contact
        2.Modifier un contact
        3.Supprimer un contact
        4.Montrer mes contacts
        5.quitter
        """)

        if value == "1":
            name = ""
            while name == "":
                name = input("Comment s'appelle votre contact?")
            agenda.add_contact(Contact(name=name))

        if value == "2":
            show_contact_options(agenda)

        if value == "3":
            index = -1
            while not (0 <= index < len(agenda.get_contacts())):
                index = int(input("""Lequel voulez-vous supprimer?
                            (PS: Marquez 0 pour le premier item, etc..."""))
            contact = agenda.get_contacts()[index]
            print(f"Vous voulez supprimer le contact {contact.get_name()}")
            agenda.remove_contact(agenda.get_contacts()[index])
            print(f"contact supprimé")

        if value == "4":
            show_contacts(agenda.get_contacts())

        if value == "5":
            print("bye bye!")
            return


# check si user exist -> salut <user>
print("Salut user!")
show_agenda()
