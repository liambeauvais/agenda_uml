from db import create_connection

conn = create_connection("db.db")
_cursor = conn.cursor()
table = "Contact"

def initTable():
    _cursor.execute("CREATE TABLE IF NOT EXISTS "+ table+" (id INTEGER PRIMARY KEY AUTOINCREMENT, idAgenda INTEGER, nom VARCHAR(255), FOREIGN KEY(idAgenda) REFERENCES Agenda(id))")

def searchBuilder(where:dict):
    search = "SELECT * FROM " + table
    for x,y in where.items():
        print(x)
        print(y)
        search += " WHERE " + x +" = " + y
    return search

def deleteBuilder(where:dict):
    delete = "DELETE FROM " + table
    for x,y in where.items():
        delete += " WHERE " + x +" = " + y
    return delete

def getAllCD(where:dict):
    search = searchBuilder(where)
    print(search)
    return _cursor.execute(search).fetchall()

def getOneCD(where:dict):
    search = searchBuilder(where)
    search += " LIMIT 1"
    print(search)
    return _cursor.execute(search)

def createCD(idContact:int,value:str,type:str):
    create = "INSERT INTO " + table + " (idAgenda, nom) VALUES (" + str(idContact) + ", '" + value + "' , '" + type +"')"
    print(create)
    _cursor.execute(create)
    conn.commit()

def removeCD(where:dict):
    delete = deleteBuilder(where)
    print(delete)
    _cursor.execute(delete)
    conn.commit()



