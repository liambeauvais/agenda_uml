from db import create_connection

conn = create_connection("db.db")
_cursor = conn.cursor()
table = "Agenda"

def initTable():
    _cursor.execute("CREATE TABLE IF NOT EXISTS "+ table+" (id INTEGER PRIMARY KEY AUTOINCREMENT, nom VARCHAR(255)")

def searchBuilder(where:dict):
    search = "SELECT * FROM " + table
    for x,y in where.items():
        print(x)
        print(y)
        search += " WHERE " + x +" = " + y
    return search

def deleteBuilder(where:dict):
    delete = "DELETE FROM " + table
    for x,y in where.items():
        delete += " WHERE " + x +" = " + y
    return delete

def getAllAgenda(where:dict):
    search = searchBuilder(where)
    print(search)
    return _cursor.execute(search).fetchall()

def getOneAgenda(where:dict):
    search = searchBuilder(where)
    search += " LIMIT 1"
    print(search)
    return _cursor.execute(search)

def createAgenda(nom:str):
    create = "INSERT INTO " + table + " (nom) VALUES ('" +nom+"')"
    print(create)
    _cursor.execute(create)
    conn.commit()

def removeAgenda(where:dict):
    delete = deleteBuilder(where)
    print(delete)
    _cursor.execute(delete)
    conn.commit()