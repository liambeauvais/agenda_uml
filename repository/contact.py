from db import create_connection

conn = create_connection("db.db")
_cursor = conn.cursor()
table = "Contact_detail"

def initTable():
    _cursor.execute("CREATE TABLE IF NOT EXISTS "+ table+" (id INTEGER PRIMARY KEY AUTOINCREMENT, idContact INTEGER, value VARCHAR(255), type VARCHAR(20), FOREIGN KEY(idContact) REFERENCES Contact(id))")

def searchBuilder(where:dict):
    search = "SELECT * FROM " + table
    for x,y in where.items():
        print(x)
        print(y)
        search += " WHERE " + x +" = " + y
    return search

def deleteBuilder(where:dict):
    delete = "DELETE FROM " + table
    for x,y in where.items():
        delete += " WHERE " + x +" = " + y
    return delete

def getAllContact(where:dict):
    search = searchBuilder(where)
    print(search)
    return _cursor.execute(search).fetchall()

def getOneContact(where:dict):
    search = searchBuilder(where)
    search += " LIMIT 1"
    print(search)
    return _cursor.execute(search)

def createContact(idAgenda:int,nom:str):
    create = "INSERT INTO " + table + " (idAgenda, nom) VALUES (" + str(idAgenda) + ", '" + nom + "')"
    print(create)
    _cursor.execute(create)
    conn.commit()

def removeContact(where:dict):
    delete = deleteBuilder(where)
    print(delete)
    _cursor.execute(delete)
    conn.commit()