# Agenda From UML
Implémentation du diagramme UML disponible à l'adresse suivante:\
https://gitlab.com/docusland-courses/uml/agenda-create-from-uml

## Installation
```
mkdir Projets
cd Projets
git clone https://gitlab.com/liambeauvais/agenda_uml.git
cd agenda_uml
pip install -r requirements.txt
```


## Lancer sur l'environnement de dev
Suite à l'installation ouvrir PyCharm ou tout ide avec un environnement python fonctionnel.
Puis lancer une session de débogage sur le fichier main.py.

## UML
![img.png](img.png)

## Contributeurs

LE PAVEC Malo @Malo-LPV\
BEAUVAIS Liam @liambeauvais\
MEINARD Titouan @3MPTYB0Y


